(function() {
    var Brands = require('../data_access/brands').Brands;
    var Managers = require('./manager_create').Managers;
    

    function createBrand(rqst, res, next, helpers) {
            if(rqst.body.brand_name === undefined){
                console.log('error')
            }
            else{
                    var date = new Date();
                    var brand_name = rqst.body.brand_name.trim();
                    var created_date = date.toISOString();

                    if(brand_name !== ""  && brand_name !== null)
                    {
                        Brands.createBrands({brand_name,created_date}, {}, helpers, function(response) {
                        res.json(response);
                         })
                    }
                    else
                    {
                        console.log("Please insert valid input")
                        console.error();
                    }
            }       
    }

    function getBrand(rqst, res, next, helpers) {                  
        Brands.getBrands({}, {}, helpers, function(response) {
            res.json(response)
        })
    }
    exports.createBrand = createBrand;
    exports.getBrand = getBrand;
})()