(function() {
    function getManager(rqst, res, next, helpers) {

        const Managers = require('../data_access/managers').Managers;

        Managers.getManagersList({}, {}, helpers, function(response) {

            res.json(response)
        })
    }


    function createManagers(rqst, res, next, helpers) {
        const Managers = require('../data_access/managers').Managers;

        const bcrypt = require('bcrypt');
        var password = rqst.body.password;
        var username = rqst.body.username;
        var uuid = helpers.generateUUID();
        var is_verified = false;
        var brand_id = rqst.body.brand_id;
        var type = rqst.body.type;
        var has_access = false;

        var mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var passwordFormate = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;


        if (!username.match(mailFormat)) {
            res.send("*Invailid Email Address");
        } else if (!password.match(passwordFormate)) {
            res.send("*at least one number, one lowercase, one uppercase and at least Six characters");
        } else {
            var hash = bcrypt.hashSync(password, 10);
            if (brand_id == "" || brand_id.length <= 15) {
                res.send("*Invalid Brand ID");
            }
            if (type != "1" && type != "2") {
                res.send("*Invalid type..! Please put 1 & 2");
            }

            Managers.createManagers({ "username": username, "password": hash, "uuid": uuid, "is_verified": is_verified, "brand_id": brand_id, "type": type, "has_access": has_access }, {}, helpers, function(response) {
                if (is_verified === false) {
                    helpers.sendEmail(username, uuid);
                    res.json(response)
                }

            })
        }
    }


    exports.Managers = {
        getManager: getManager,
        createManagers: createManagers
    }
})()
